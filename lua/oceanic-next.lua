-- Theme: oceanic
-- Author: Glepnir
-- License: MIT
-- Source: http://github.com/glepnir/oceanic-nvim

local transparent = false;

local oceanic = {
	base00  = '#1b2b34';
	base01  = '#343d46';
	base02  = '#4f5b66';
	base03  = '#65737e';
	base04  = '#a7adba';
	base05  = '#c0c5ce';
	base06  = '#cdd3de';
	base07  = '#d8dee9';

	red     = '#ec5f67';
	orange  = '#f99157';
	yellow  = '#fac863';
	green   = '#99c794';
	cyan    = '#62b3b2';
	blue    = '#6699cc';
	purple  = '#c594c5';
	brown   = '#ab7967';
	white   = '#ffffff';
	none    = 'NONE';
}

function oceanic.terminal_color()
	vim.g.terminal_color_0 = oceanic.base00
	vim.g.terminal_color_1 = oceanic.red
	vim.g.terminal_color_2 = oceanic.green
	vim.g.terminal_color_3 = oceanic.yellow
	vim.g.terminal_color_4 = oceanic.blue
	vim.g.terminal_color_5 = oceanic.purple
	vim.g.terminal_color_6 = oceanic.cyan
	vim.g.terminal_color_7 = oceanic.base05
	vim.g.terminal_color_8 = oceanic.base03
	vim.g.terminal_color_9 = oceanic.red
	vim.g.terminal_color_10 = oceanic.green
	vim.g.terminal_color_11 = oceanic.yellow
	vim.g.terminal_color_12 = oceanic.blue
	vim.g.terminal_color_13 = oceanic.purple
	vim.g.terminal_color_14 = oceanic.cyan
	vim.g.terminal_color_15 = oceanic.base05
end

function oceanic.highlight(group, color)
	local style = color.style and 'gui=' .. color.style or 'gui=NONE'
	local fg = color.fg and 'guifg=' .. color.fg or 'guifg=NONE'
	local bg = not transparent and color.bg and 'guibg=' .. color.bg or 'guibg=NONE'
	local sp = color.sp and 'guisp=' .. color.sp or ''
	vim.api.nvim_command('highlight ' .. group .. ' ' .. style .. ' ' .. fg ..
                             ' ' .. bg..' '..sp)
end


function oceanic.load_syntax()
	local syntax = {
		Error = {fg=oceanic.red,bg=oceanic.base00};
		Bold = {style='bold'};
		Debug = {fg=oceanic.red};
		Directory = {fg=oceanic.blue};
		ErrorMsg = {fg=oceanic.red,bg=oceanic.base00};
		Exception = {fg=oceanic.red};
		FoldColumn = {fg=oceanic.blue,bg=oceanic.base00};
		Folded = {fg=oceanic.base03,oceanic.base01,style='italic'};
		IncSearch = {fg=oceanic.base01,bg=oceanic.orange,style=oceanic.none};
		Italic = {style='italic'};

		Macro = {fg=oceanic.red};
		MatchParen = {fg=oceanic.base05,bg=oceanic.base03};
		ModeMsg = {fg=oceanic.green};
		MoreMsg = {fg=oceanic.green};
		Question = {fg=oceanic.blue};
		Search = {fg=oceanic.base03,bg=oceanic.yellow};
		SpecialKey = {fg=oceanic.base03};
		TooLong = {fg=oceanic.red};
		Underlined = {fg=oceanic.red};
		Visual = {bg=oceanic.base02};
		VisualNOS = {fg=oceanic.red};
		WarningMsg = {fg=oceanic.red};
		WildMenu = {fg=oceanic.base07,bg=oceanic.blue};
		Title = {fg=oceanic.blue};
		Conceal = {fg=oceanic.blue,bg=oceanic.base00};
		Cursor = {fg=oceanic.base00,bg=oceanic.base05};
		NonText = {fg=oceanic.base03};
		Normal = {fg=oceanic.base07, bg=oceanic.base00};
		EndOfBuffer = {fg=oceanic.base05, bg=oceanic.base00};
		LineNr = {fg=oceanic.base03, bg=oceanic.base00};
		SignColumn = {fg=oceanic.base00, bg=oceanic.base00};
		StatusLine = {fg=oceanic.base01, bg=oceanic.base03};
		StatusLineNC = {fg=oceanic.base03, bg=oceanic.base01};
		VertSplit = {fg=oceanic.base00, bg=oceanic.base02};
		ColorColumn = {bg=oceanic.base01};
		CursorColumn = {bg=oceanic.base01};
		CursorLine = {bg=oceanic.base01};
		CursorLineNR = {fg=oceanic.base03, bg=oceanic.base00};
		CursorLineNr = {fg=oceanic.base03, bg=oceanic.base01};
		PMenu = {fg=oceanic.base04, bg=oceanic.base01};
		PMenuSel = {fg=oceanic.base07, bg=oceanic.blue};
		PmenuSbar = {bg=oceanic.base02};
		PmenuThumb = {bg=oceanic.base07};
		TabLine = {fg=oceanic.base03, bg=oceanic.base01};
		TabLineFill = {fg=oceanic.base03, bg=oceanic.base01};
		TabLineSel = {fg=oceanic.green,  bg=oceanic.base01};
		helpExample = {fg=oceanic.yellow};
		helpCommand = {fg=oceanic.yellow};

		-- Standard syntax highlighting
		Boolean = {fg=oceanic.orange};
		Character = {fg=oceanic.red};
		Comment = {fg=oceanic.base03,style='italic'};
		Conditional = {fg=oceanic.purple};
		Constant = {fg=oceanic.orange};
		Define = {fg=oceanic.purple};
		Delimiter = {fg=oceanic.brown};
		Float = {fg=oceanic.orange};
		Function = {fg=oceanic.blue};

		Identifier = {fg=oceanic.cyan};
		Include = {fg=oceanic.blue};
		Keyword = {fg=oceanic.purple};

		Label = {fg=oceanic.yellow};
		Number = {fg=oceanic.orange};
		Operator = {fg=oceanic.base05};
		PreProc = {fg=oceanic.yellow};
		Repeat = {fg=oceanic.yellow};
		Special = {fg=oceanic.cyan};
		SpecialChar = {fg=oceanic.brown};
		Statement = {fg=oceanic.red};
		StorageClass = {fg=oceanic.yellow};
		String = {fg=oceanic.green};
		Structure = {fg=oceanic.purple};
		Tag = {fg=oceanic.yellow};
		Todo = {fg=oceanic.yellow, bg=oceanic.base01};
		Type = {fg=oceanic.yellow};
		Typedef = {fg=oceanic.yellow};

		NvimInternalError = {bg=oceanic.red};
	}
	return syntax
end

function oceanic.load_plugin_syntax()
	local plugin_syntax = {
		TSInclude           = {fg=oceanic.cyan};
		TSPunctBracket      = {fg=oceanic.cyan};
		TSPunctDelimiter    = {fg=oceanic.base07};
		TSParameter         = {fg=oceanic.base07};
		TSType              = {fg=oceanic.blue};
		TSFunction          = {fg=oceanic.cyan};

		TSTagDelimiter      = {fg=oceanic.cyan};
		TSProperty          = {fg=oceanic.yellow};
		TSMethod            = {fg=oceanic.blue};
		TSParameter         = {fg=oceanic.yellow};
		TSConstructor       = {fg=oceanic.base07};
		TSVariable          = {fg=oceanic.base07};
		TSOperator          = {fg=oceanic.base07};
		TSTag               = {fg=oceanic.base07};
		TSKeyword           = {fg=oceanic.purple};
		TSKeywordOperator   = {fg=oceanic.purple};
		TSVariableBuiltin   = {fg=oceanic.red};
		TSLabel             = {fg=oceanic.cyan};

		DiffAdd             = {fg=oceanic.green,    bg=oceanic.base01,  style='bold'};
		DiffChange          = {fg=oceanic.base03,   bg=oceanic.base01};
		DiffDelete          = {fg=oceanic.red,      bg=oceanic.base01};
		DiffText            = {fg=oceanic.blue,     bg=oceanic.base01};
		DiffAdded           = {fg=oceanic.base07,   bg=oceanic.green,   style='bold'};
		DiffFile            = {fg=oceanic.red,      bg=oceanic.base00};
		DiffNewFile         = {fg=oceanic.green,    bg=oceanic.base00};
		DiffLine            = {fg=oceanic.blue,     bg=oceanic.base00};
		DiffRemoved         = {fg=oceanic.base07,   bg=oceanic.red,     style='bold'};

		vimCommentTitle     = {fg=oceanic.grey,style='bold'};
		vimLet              = {fg=oceanic.orange};
		vimVar              = {fg=oceanic.cyan};
		vimFunction         = {fg=oceanic.redwine};
		vimIsCommand        = {fg=oceanic.fg};
		vimCommand          = {fg=oceanic.blue};
		vimNotFunc          = {fg=oceanic.violet,style='bold'};
		vimUserFunc         = {fg=oceanic.yellow,style='bold'};
		vimFuncName         = {fg=oceanic.yellow,style='bold'};

		gitcommitSummary    = {fg = oceanic.red};
		gitcommitUntracked  = {fg = oceanic.grey};
		gitcommitDiscarded  = {fg = oceanic.grey};
		gitcommitSelected   = { fg=oceanic.grey};
		gitcommitUnmerged   = { fg=oceanic.grey};
		gitcommitOnBranch   = { fg=oceanic.grey};
		gitcommitArrow      = {fg = oceanic.grey};
		gitcommitFile       = {fg = oceanic.dark_green};

		VistaBracket        = {fg=oceanic.grey};
		VistaChildrenNr     = {fg=oceanic.orange};
		VistaKind           = {fg=oceanic.purpl};
		VistaScope          = {fg=oceanic.red};
		VistaScopeKind      = {fg=oceanic.blue};
		VistaTag            = {fg=oceanic.magenta,style='bold'};
		VistaPrefix         = {fg=oceanic.grey};
		VistaColon          = {fg=oceanic.magenta};
		VistaIcon           = {fg=oceanic.yellow};
		VistaLineNr         = {fg=oceanic.fg};

		GitGutterAdd        = {fg=oceanic.dark_green};
		GitGutterChange     = {fg=oceanic.blue};
		GitGutterDelete     = {fg=oceanic.red};
		GitGutterChangeDelete={fg=oceanic.violet};

		GitSignsAdd         = {fg=oceanic.dark_green};
		GitSignsChange      = {fg=oceanic.blue};
		GitSignsDelete      = {fg=oceanic.red};
		GitSignsAddNr       = {fg=oceanic.dark_green};
		GitSignsChangeNr    = {fg=oceanic.blue};
		GitSignsDeleteNr    = {fg=oceanic.red};
		GitSignsAddLn       = {bg=oceanic.bg_popup};
		GitSignsChangeLn    = {bg=oceanic.bg_highlight};
		GitSignsDeleteLn    = {bg=oceanic.bg1};

		SignifySignAdd      = {fg=oceanic.dark_green};
		SignifySignChange   = {fg=oceanic.blue};
		SignifySignDelete   = {fg=oceanic.red};

		dbui_tables         = {fg=oceanic.blue};

		CursorWord0         = {bg=oceanic.currsor_bg};
		CursorWord1         = {bg=oceanic.currsor_bg};

		NvimTreeFolderName  = {fg=oceanic.blue};
		NvimTreeRootFolder  = {fg=oceanic.red,style='bold'};
		NvimTreeSpecialFile = {fg=oceanic.fg,bg=oceanic.none,style=oceanic.none};

		TelescopeBorder         = {fg=oceanic.purple};
		TelescopePromptBorder   = {fg=oceanic.yellow};
		TelescopeResultBorder   = {fg=oceanic.purple};
		TelescopePreviewBorder  = {fg=oceanic.purple};

		TelescopeMatching       = {fg=oceanic.blue};
		TelescopePromptPrefix   = {fg=oceanic.blue};

		TelescopeSelection      = {fg=oceanic.yellow, style='bold'};
		TelescopeSelectionCaret = {fg=oceanic.red};
		TelescopeMultiSelection = {fg=oceanic.purple};
		TelescopeNormal         = {fg=oceanic.white};

		LspDiagnosticsSignError             = {fg=oceanic.red};
		LspDiagnosticsSignWarning           = {fg=oceanic.yellow};
		LspDiagnosticsSignInformation       = {fg=oceanic.blue};
		LspDiagnosticsSignHint              = {fg=oceanic.cyan};

		LspDiagnosticsVirtualTextError      = {fg=oceanic.red};
		LspDiagnosticsVirtualTextWarning    = {fg=oceanic.yellow};
		LspDiagnosticsVirtualTextInformation= {fg=oceanic.blue};
		LspDiagnosticsVirtualTextHint       = {fg=oceanic.cyan};

		LspDiagnosticsUnderlineError        = {style="undercurl",sp=oceanic.red};
		LspDiagnosticsUnderlineWarning      = {style="undercurl",sp=oceanic.yellow};
		LspDiagnosticsUnderlineInformation  = {style="undercurl",sp=oceanic.blue};
		LspDiagnosticsUnderlineHint         = {style="undercurl",sp=oceanic.cyan};

		LspDiagnosticsDefaultError          = {fg=oceanic.red, bg=oceanic.base00};
		LspDiagnosticsDefaultWarning        = {fg=oceanic.yellow};
		LspDiagnosticsDefaultInformation    = {fg=oceanic.blue};
		LspDiagnosticsDefaultHint           = {fg=oceanic.cyan};
	}
	return plugin_syntax
end

function oceanic.colorscheme()
	vim.api.nvim_command('hi clear')

	if vim.fn.exists('syntax_on') then
		vim.api.nvim_command('syntax reset')
	end

	vim.o.background = 'dark'
	vim.o.termguicolors = true
	vim.g.colors_name = 'oceanic-next'

	local syntax = oceanic.load_syntax()

	for group,colors in pairs(syntax) do
		oceanic.highlight(group,colors)
	end

	local syntax = oceanic.load_plugin_syntax()
	for group,colors in pairs(syntax) do
		oceanic.highlight(group,colors)
	end
end

oceanic.colorscheme()

return oceanic
